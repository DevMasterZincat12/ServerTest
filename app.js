const express = require('express');

const port = 8000;

const app = express();

app.use(express.json());

app.use('/', (req,res)=>{
    res.status(200).send({message:"hi"});
});

app.use((req, res, next) => {
    var requestedUrl = req.protocol + '://' + req.get('Host') + req.url;

    return res.sendStatus(404);
});

// error handler
app.use((err, req, res, next) => {
    res.sendStatus(500);
});

app.listen(port, ()=>{
    console.log('Server started at '+ port);
})